'use strict';

let path = require('path');
require.main.dirname = path.dirname(require.main.filename);

let core            = require('core-libs');
let bootstrap       = core.bootstrap;
let configFactory   = core.configFactory;
let config          = configFactory.getConfig('core');
let logger          = core.loggerFactory.getLogger(__filename);
let moduleAdder     = core.functions.moduleAddFunction();

let sever = require('./src/index');
let modules = config.get('modules');

for (let idx in modules)
{
    module = modules[idx];
    module.run = moduleAdder(module);
}

bootstrap.finishedInit()
    .then(() => {

        for (let idx in modules)
        {
            module = modules[idx];
            sever.app.use(module['mount-path'], module.run.app);
        }

        sever.install404();

        sever.app.listen(config.appPort, function() {
            logger.info(`App Core started on localhost:${config.appPort}`);
        });
    })
    .catch((err) => {
        logger.fatal('Could not run app, err:', err);
    });

