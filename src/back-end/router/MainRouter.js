'use strict';

let express = require('express');

let core = require('core-libs');
let bootstrap = core.bootstrap;
let logger = core.loggerFactory.getLogger(__filename);
let CoreRouter = core.router;
let renderEngine = core.renderEngine;

let apiRouter = require('./ApiRouter');

class MainRouter extends CoreRouter {
    init() {
        return super.init()
            .then(() => {

                this.app.engine('html', renderEngine.getHtmlEngine);
                this.app.engine('bundle', renderEngine.getBundleEngine);

                // Install routes (order important)
                this.app
                    .use('/static/js',       express.static(renderEngine.getStaticPath('core', 'js')))
                    .use('/static/css',      express.static(renderEngine.getStaticPath('core', 'css')))
                    .use('/static/fonts',    express.static(renderEngine.getStaticPath('core', 'fonts')))
                    .use('/static/images',   express.static(renderEngine.getImagePath('core')))

                    .use('/api', apiRouter.getRouter())
                ;

                return Promise.resolve();

            })
            .catch((err) => {
                logger.error('Could not start server, err:', err);
                return Promise.reject(err);
            });
    }
}

let _singleton = new MainRouter();
bootstrap.register(__filename, _singleton, _singleton.init);
module.exports = _singleton;