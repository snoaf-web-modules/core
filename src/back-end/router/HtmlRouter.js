'use strict';

let core = require('core-libs');
let bootstrap = core.bootstrap;
let logger = core.loggerFactory.getLogger(__filename);
let hitch = core.functions.hitch;
let renderEngine = core.renderEngine;
let CoreRouter = core.router;

let HomeRouter = require('snoaf/router/HtmlRouter');

class HtmlRouter extends CoreRouter {
    init() {
        return super.init()
            .then(() => {
                this.start();
                return Promise.resolve();
            });
    }

    start() {
        this.app.engine('html', renderEngine.getEngine);

        // Install routes
        this.app
            .use('/', HomeRouter.router)
        ;

    }
}

let _singleton = new HtmlRouter();
bootstrap.register(__filename, _singleton, _singleton.init);
module.exports = _singleton;