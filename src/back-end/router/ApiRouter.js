'use strict';

let core = require('core-libs');
let bootstrap = core.bootstrap;
let coreRouter  = core.router;

let testApi     = require('../api/TestApi');

class ApiRouter extends coreRouter {
    init() {
        return super.init()
            .then(() => {
                // Install routes
                this.app
                    .use('/test', testApi.REST)
                    .all('*', this._invalid)
                ;

                return Promise.resolve();
            });
    }

    apiTest(req, res, next) {
        res.send('Hello World');
    }

    _invalid(req, res, next) {
        next();
    }
}

let _singleton = new ApiRouter();
bootstrap.register(__filename, _singleton, _singleton.init);
module.exports = _singleton;