'use strict';

let express = require('express');

let logger = require('../../lib/LoggerFactory').getLogger(__filename);

let CoreApi = require('../../lib/api/CoreApi');

class TestApi extends CoreApi {
    constructor() {
        super();

    }

    _get(req, res, next) {
        res.send({
            upcoming: [{
                title: 'Birthday',
                'time-frame': '7:00pm 11/01/18',
                location: 'The Bar Down the Road'
            },
                {
                    title: 'Party',
                    'time-frame': '9:00pm 12/01/18',
                    location: 'My House'
                }],
            invited: [{
                title: 'Birthday',
                'time-frame': '7:00pm 11/01/18',
                location: 'The Bar Down the Road'
            }],
            nearby: [{
                title: 'Birthday',
                'time-frame': '7:00pm 11/01/18',
                location: 'The Bar Down the Road'
            }]
        });
    }

}

let _singleton = new TestApi();
module.exports = _singleton;