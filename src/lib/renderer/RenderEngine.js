'use strict';

let fs = require('fs');
let path = require('path');

let bootstrap       = require('../Bootstrap');
let configFactory   = require('../ConfigFactory');
let logger          = require('../LoggerFactory').getLogger(__filename);

class RenderEngine {

    getPagePath(module, fileName) {
        fileName = fileName || '';
        let config = configFactory.getConfig(module);
        return path.join(config.getPath('content-path'), 'views', 'pages', fileName);
    }

    getStaticPath(module, location) {
        location = location || '';
        let config = configFactory.getConfig(module);
        return path.join(config.getPath('content-path'), location);
    }

    getImagePath(module) {
        let config = configFactory.getConfig(module);
        let imagePath = config.getPath('image-path') ||
            (config.getPath('content-path') + '/assets/images');
        return path.join(imagePath);
    }

    getStaticImagePath(module, location) {
        let config = configFactory.getConfig(module);
        let imagePath = config.getPath('image-path') ||
            (config.getPath('content-path') + '/assets/images');
        return path.join(imagePath, location);
    }

    _getPartialPath(module, fileName) {
        let config = configFactory.getConfig(module);
        return path.join(config.getPath('content-path'), 'views', 'partials', fileName);
    }

    getHtmlEngine(filePath, ctx, callback) {
        RenderEngine._getFile(filePath)
            .then((file) => {return RenderEngine._renderHtmlFile(file, ctx);})
            .then((text) => {
                logger.debug('Page has been rendered; ', filePath);
                callback(null, text);
            })
            .catch((err) => {
                logger.error('Could not render page; error: ', err);
                callback('Could not render page; error: ' + err, '');
            });
    }

    getBundleEngine(filePath, ctx, callback) {
        RenderEngine._getFile(filePath)
            .then((file) => {return RenderEngine._renderBundleFile(file, ctx);})
            .then((text) => {
                logger.debug('Page has been rendered; ', filePath);
                callback(null, text);
            })
            .catch((err) => {
                logger.error('Could not render page; error: ', err);
                callback('Could not render page; error: ' + err, '');
            });
    }

    static _getFile(filePath) {
        return new Promise((resolve, reject) => {
            fs.readFile(filePath, null, (err, content) => {
                if (err)
                {
                    logger.warn(err);
                    reject();
                }
                else
                {
                    resolve(content.toString());
                }
            })
        });
    }

    static _renderHtmlFile(fileContents, ctx) {
        return Promise.resolve(fileContents)
            .then((renderedContents) => {return RenderEngine._forReplace(renderedContents, ctx);})
            .then((renderedContents) => {return RenderEngine._ifReplace(renderedContents, ctx);})
            .then((renderedContents) => {return RenderEngine._inlineConditionalReplace(renderedContents, ctx);})
            .then((renderedContents) => {return RenderEngine._includePartialReplace(renderedContents, ctx);})
            .then((renderedContents) => {return RenderEngine._includeFileReplace(renderedContents, ctx);})
            .then((renderedContents) => {return RenderEngine._ctxReplace(renderedContents, ctx);});
    }

    static _renderBundleFile(fileContents, ctx) {
        return Promise.resolve(fileContents)
            .then((renderedContents) => {return RenderEngine._includeBundleReplace(renderedContents, ctx);});
    }

    static _replacer(fileContents, search, replacementFunction) {
        let matchStack = [];

        if(!fileContents)
        {
            return Promise.resolve('');
        }

        fileContents.replace(search, function() {
            let args = Array.from(arguments);
            let match = args[0];
            let p = args.slice(1, -2);

            let replacedText = replacementFunction(match, p)
                .then((text) => {

                    if (typeof text == 'string')
                    {
                        text = text.trim();
                    }

                    return {
                        initial: match,
                        replacement: text
                    };
                });

            matchStack.push(replacedText);
            return match;

        });

        return Promise.all(matchStack)
            .then((matches) => {
                for (let idx in matches)
                {
                    let match = matches[idx];
                    fileContents = fileContents.replace(match.initial, match.replacement);
                }
                return Promise.resolve(fileContents);
            });
    }

    static _ctxTreeWalker(pathArray, ctx) {
        if (pathArray.length == 0)
        {
            return ctx;
        }
        else if (ctx && (ctx.hasOwnProperty(pathArray[0])))
        {
            let newCtx = ctx[pathArray.shift()];
            return RenderEngine._ctxTreeWalker(pathArray, newCtx);
        }
        else
        {
            logger.warn(`Invalid Object; could not find: ${pathArray[0]} in ctx: ${ctx}`);
            return null;
        }
    };

    static _ctxEval(inputString, ctx) {
        if (!inputString || inputString.trim() == '')
        {
            logger.warn('A null input string has been given to ctxEval; ', inputString);
            return null;
        }

        inputString = inputString.trim();

        try
        {
            return JSON.parse('{"value": ' + inputString + '}').value;
        }
        catch (e)
        {
            // This means the object is not JSON and must be an object
            // Continue with processing
        }

        let compareBreakdown = /([\s\S]*?)\s*(\=\=|\>|\>\=|\<|\<\=|\!\=)\s*([\s\S]*)/g.exec(inputString);
        if (compareBreakdown && compareBreakdown.length > 1)
        {
            compareBreakdown[1] = RenderEngine._ctxEval(compareBreakdown[1], ctx);
            compareBreakdown[1] = typeof compareBreakdown[1] == 'string' ? '"' + compareBreakdown[1] + '"' : compareBreakdown[1];
            compareBreakdown[3] = RenderEngine._ctxEval(compareBreakdown[3], ctx);
            compareBreakdown[3] = typeof compareBreakdown[3] == 'string' ? '"' + compareBreakdown[3] + '"' : compareBreakdown[3];
            compareBreakdown.shift();

            let evalString = compareBreakdown.join(' ');
            let evaluation = eval(evalString);
            return evaluation;
        }

        let functionBreakdown = /([^\(\)\,]*)(?:\(([^\(\)]*)\))?/g.exec(inputString);
        let objectAsString = functionBreakdown[1];
        let argsAsString = functionBreakdown[2] || '';

        let objectAsArray = objectAsString.split('.');
        let ctxValue = RenderEngine._ctxTreeWalker(objectAsArray, ctx);

        let args = argsAsString
            .split(',')
            .map((value) => {
                if(value != '')
                {
                    return RenderEngine._ctxEval(value, ctx);
                }
                else
                {
                    return null;
                }
            });



        if (typeof ctxValue == 'function')
        {
            return ctxValue.apply(ctx, args);
        }

        return ctxValue;
    }

    static _ctxReplace(fileContents, ctx) {
        let ctxElementSearch = /\{\%\s*([\s\S]*?)\s*\%\}/g;

        let ctxReplacementFunction = function(match, args) {
            let objectAsString = args[0];

            let returnValue = RenderEngine._ctxEval(objectAsString, ctx);

            if (!returnValue)
            {
                returnValue = '';
            }

            return Promise.resolve(returnValue);
        };

        return RenderEngine._replacer(fileContents, ctxElementSearch, ctxReplacementFunction);
    }

    static _inlineConditionalReplace(fileContents, ctx) {
        let ctxElementSearch = /\{\%\s*([\s\S]*?)\s*\?\s*\'(.*?)\'\s*\:\s*\'(.*?)\'\s*\%\}/g;

        let inlineConditionalReplacementFunction = function (match, args) {
            let conditionalObject = args[0];
            let positiveResult = args[1], negativeResult = args[2];

            let conditionalResult = self.ctxEval(conditionalObject, ctx);

            if (conditionalResult)
            {
                return RenderEngine._renderFile(positiveResult, ctx);
            }
            else
            {
                return RenderEngine._renderFile(negativeResult, ctx);
            }

        };

        return RenderEngine._replacer(fileContents, ctxElementSearch, inlineConditionalReplacementFunction);
    }

    static _ifReplace(fileContents, ctx) {
        let ctxElementSearch = /\{\%\s*if\s+([\s\S]*?)\s*\%\}([\s\S]*?)(?:\{\%\s*else\s*\%\}([\s\S]*?))?\{\%\s*end\sif\s*\%\}/g;

        let ifReplacementFunction = function(match, args) {
            let conditional = args[0];
            let positiveResult = args[1], negativeResult = args[2];

            let conditionalResult = RenderEngine._ctxEval(conditional, ctx);

            if (conditionalResult)
            {
                return RenderEngine._renderFile(positiveResult, ctx);
            }
            else
            {
                return RenderEngine._renderFile(negativeResult, ctx);
            }
        };

        return RenderEngine._replacer(fileContents, ctxElementSearch, ifReplacementFunction);
    }

    static _forReplace(fileContents, ctx) {
        let ctxElementSearch = /\{\%\s*for\s+([\s\S]*?)\s+in\s+([\s\S]*?)\s*\%\}([\s\S]*?)\{\%\s*end\s+for\s*\%\}/g;

        let forReplacementFunction = function(match, args) {
            let iteratorName = args[0].split(',')[0].trim();
            let iteratorKey = (args[0].split(',')[1] || '').trim();
            let objectName = args[1], content = args[2];
            let renderedQueue = [];
            let ctxObject = RenderEngine._ctxEval(objectName, ctx);

            if (typeof ctxObject != 'array' && typeof ctxObject != 'object')
            {
                logger.warn(`Property '${ctxObject}' is not an iterable object`);
                return RenderEngine._renderFile(content, ctx);
            }
            else if (!ctxObject)
            {
                logger.warn(`Property '${ctxObject}' does not exist in this context`);
                return RenderEngine._renderFile(content, ctx);
            }

            for (let idx in ctxObject)
            {
                let iterationCtx = merge(ctx, {});
                iterationCtx[iteratorName] = ctxObject[idx];
                iterationCtx[iteratorKey] = idx;

                renderedQueue.push(RenderEngine._renderFile(content, iterationCtx));
            }

            return Promise.all(renderedQueue).then((result) => {
                return result.join('\n');
            });

        };

        return RenderEngine._replacer(fileContents, ctxElementSearch, forReplacementFunction);
    }

    static _includePartialReplace(fileContents, ctx) {
        let self = new RenderEngine();

        let includeSearch = /\{\%\s*include\s*([\'\"\`])([\/A-Za-z0-9]*(?:\.([A-Za-z0-9])*)?)\1\s*\%\}/g;

        let includeReplacementFunction = function(match, args) {
            let filePath = args[1], extension = args[2];
            let partialFile = self._getPartialPath(ctx.module, filePath + (extension == undefined ? '.html' : ''));

            return RenderEngine._getFile(partialFile)
                .then((partialContents) => {return RenderEngine._renderHtmlFile(partialContents, ctx);});

        };

        return RenderEngine._replacer(fileContents, includeSearch, includeReplacementFunction);
    }

    static _includeFileReplace(fileContents, ctx) {
        let self = new RenderEngine();

        let includeSearch = /\{\%\s*includeFile\s*([\-A-Za-z0-9]*)\s*([\'\"\`])([\/\-A-Za-z0-9]*\.[A-Za-z0-9]*)\2\s*\%\}/g;

        let includeReplacementFunction = function(match, args) {
            let filePath = args[2];
            let module = (args[0] == '' ? ctx.module : args[0]);

            let file = self.getStaticPath(module, filePath);

            return RenderEngine._getFile(file);

        };

        return RenderEngine._replacer(fileContents, includeSearch, includeReplacementFunction);
    }

    static _includeBundleReplace(fileContents, ctx) {
        let includeSearch = /\{\%\s*include\s*([\'\"\`])([\/A-Za-z0-9]*(?:\.([A-Za-z0-9])*)?)\1\s*\%\}/g;

        let includeReplacementFunction = function(match, args) {
            let filePath = args[1], extension = args[2];
            let staticFile = self.getStaticPath(ctx.module, filePath) + ((extension == undefined) ? '.html' : '');

            return RenderEngine._getFile(staticFile)
                .then((staticContents) => {return RenderEngine._renderFile(staticContents, ctx);});

        };

        return RenderEngine._replacer(fileContents, includeSearch, includeReplacementFunction);
    }

}

let _singleton = new RenderEngine();
module.exports = _singleton;