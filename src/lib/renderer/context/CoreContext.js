'use strict';

let configFactory = require('../../ConfigFactory');
let logger = require('../../LoggerFactory').getLogger(__filename);

class _CoreContext {
    constructor(req) {
        let ctx = req.ctx;
        let config = configFactory.getConfig(req.ctx.module);

        this.path = req.route.path;
        this.params = req.params;

        this.pageName = ctx.pageName || 'Home';
        this.projectName = config.get('project-name');
        this.title = function() {
            return this.projectName + ' - ' + this.pageName;
        };

        this.makeFunctionsProperties();
    }

    // Add functions as properties instead of properties of the prototype
    makeFunctionsProperties() {
        let protoClimber = this;
        while(protoClimber = Object.getPrototypeOf(protoClimber))
        {
            let functionList = Object.getOwnPropertyNames(protoClimber);
            for (let idx in functionList) {
                let functionName = functionList[idx];
                if (functionName != 'constructor' &&
                    functionName != 'makeFunctionsProperties' &&
                    !this.hasOwnProperty(functionName) &&
                    !functionName.match(/\_\_.*?\_\_/))
                {
                    this[functionName] = this[functionName];
                }
            }
        }
    }
}


module.exports = _CoreContext;