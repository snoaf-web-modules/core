'use strict';

let express = require('express');

let logger = require('../LoggerFactory').getLogger(__filename);

class _CoreApi {
    constructor() {
        this.RESTrouter = express.Router();

        this.RESTfunctions = {
            'get': this._get || this._invalid,
            'post': this._post || this._invalid,
            'put': this._put || this._invalid,
            'delete': this._delete || this._invalid
        };

        for (let method in this.RESTfunctions)
        {
            this.RESTrouter[method]('', this.RESTfunctions[method]);
        }

        this.RESTrouter.all('', this._invalid);
    }

    get REST() {
        return this.RESTrouter;
    }

    get get() {
        return this.getMethod('get');
    }

    get post() {
        return this.getMethod('post');
    }

    get put() {
        return this.getMethod('put');
    }

    get remove() {
        return this.getMethod('delete');
    }

    getMethod(method) {
        if (this.RESTfunctions.hasOwnProperty(method))
        {
            if (this.RESTfunctions[method] == this._invalid)
            {
                logger.debug('Invalid method(', method, ') has been assigned as a route');
            }

            return this.RESTfunctions[method];
        }

        logger.error('Non-REST function has been requested as route');
    }

    _invalid(req, res, next) {
        logger.warn('Invalid route has been accessed.');
        res.sendStatus(405);
    }
}

module.exports = _CoreApi;