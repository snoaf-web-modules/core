let path = require('path');

class CoreFunctions {
    hitch(ctx, functionName, params) {
        params = params || [];
        
        return function() {
            let allArguments = params.concat(Array.from(arguments));
            ctx[functionName].call(ctx, ...allArguments);
        }
    }

    moduleAddFunction() {
        return function (object) {
            console.log('Module added:', path.resolve(path.relative('module-dir', object.location)));
            return require(path.join('module-dir', object.location));
        };
    }
}

let _singleton = new CoreFunctions();
module.exports = _singleton;