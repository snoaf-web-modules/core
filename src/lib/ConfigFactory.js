'use strict';

let minimist = require('minimist');
let fs = require('fs');
let path = require('path');
let mainDir = path.join(require.main.dirname, '..');

class ConfigFactory {
    constructor() {
        let argv = minimist(process.argv.slice(2));
        let configFile = argv['conf'];

        this.configs = {};

        // create a core config and then more for the individual modules
        this.addConfig('core', configFile);
    }

    getConfig(configName) {
        if (this.configs.hasOwnProperty(configName))
        {
            return this.configs[configName];
        }
        else
        {
            console.log('FATAL - Could not get Config:', configName);
            process.exit(1);
        }
    }

    getConfigModules() {
        return this.configs;
    }

    addConfig(confName, confFile) {
        let conf = new Config(confFile);
        if (conf._valid())
        {
            this.configs[confName] = conf;
        }
    }
}

class Config {
    constructor(configPath) {
        let confExists = fs.existsSync(configPath);
        this.conf = null;

        if (confExists)
        {
            this.conf = JSON.parse(fs.readFileSync(configPath), {encoding: 'UTF-8'});
            console.log(__filename, ' - Config has been initialised - ', configPath);
        }
        else
        {
            console.log(__filename, ' - FATAL - Config could not be initialised - ', configPath);
        }
    }

    _valid() {
        return this.conf ? true : false;
    }

    getPath(object) {
        let thisPath = this.conf[object];
        if (thisPath && !path.isAbsolute(thisPath))
        {
            thisPath = path.resolve(path.relative('module-dir', thisPath))
        }
        return thisPath;
    }
    
    get(object) {
        return this.conf[object];
    }

    get appPort() {
        let serverOptions = this.conf['server-options'] && {
            basePort: this.conf['server-options'] && this.conf['server-options']['base-port'],
            id: this.conf['server-options'] && this.conf['server-options']['id']
        };

        if (serverOptions == null || serverOptions.basePort == null)
        {
            return;
        }

        let basePort = parseInt(serverOptions.basePort);
        let id = parseInt(serverOptions && serverOptions.id || 0);

        return basePort + id;
    }

    getValues() {
        return this.conf;
    }
}


// Create singleton
let _singleton = new ConfigFactory();

// Add config files of all sub modules
let coreConf = _singleton.getConfig('core');
let modules = coreConf.get('modules');
if (modules)
{
    for(let idx in modules)
    {
        let module = modules[idx];
        _singleton.addConfig(module.name, path.join(mainDir, module.conf));
    }
}

module.exports = _singleton;