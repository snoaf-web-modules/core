'use strict';

let mySql = require('mysql');
let squel = require('squel');

let logger      = require('../../LoggerFactory').getLogger(__filename);

class MySqlInterface {
    init(mysqlConfig) {
        let connectionObject = {
            connectionLimit: 10,
            host: 'localhost',
            port: 3306,
            user: 'admin',
            password: '',
            database: ''
        };

        if (mysqlConfig)
        {
            connectionObject = {
                connectionLimit: mysqlConfig.connectionLimit || 10,
                host: mysqlConfig.location || 'localhost',
                port: mysqlConfig.port || 3306,
                user: mysqlConfig.username || 'admin',
                password: mysqlConfig.pw || '',
                database: mysqlConfig.db || ''
            };
        }
        else
        {

        }

        this.connection = mySql.createPool(connectionObject);

        if (!this.connection)
        {
            // logger.warn('Could not connect to DB: ', mysqlConfig.location || 'localhost', ':', mysqlConfig.port || 3306);
            return Promise.reject('Could not connect to DB at: ' + connectionObject.location + ':' + connectionObject.port)
        }

        return Promise.resolve();
    }

    addRow(table, valuesObject) {
        let sql = squel.insert()
            .into(table);

        for (let key in valuesObject)
        {
            if (valuesObject.hasOwnProperty(key))
            {
                let value = valuesObject[key];
                sql = sql.set(key, value);
            }
        }

        return this.query(sql.toParam());
    }

    getRow(table, idName, id) {
        // create a way to check that keys and id are within the given table

        let sql = squel.select()
            .table(table)
            .where(idName + ' = ?', id);

        return this.query(sql.toParam());
    }

    updateRow(table, idName, id, valuesObject) {
        // create a way to check that keys and id are within the given table

        let sql = squel.update()
            .table(table)
            .where(idName + ' = ?', id);

        for (let key in valuesObject)
        {
            if (valuesObject.hasOwnProperty(key))
            {
                let value = valuesObject[key];
                sql = sql.set(key, value);
            }
        }

        return this.query(sql.toParam());
    }

    deleteRow(table, idName, id, valuesObject) {
        // create a way to check that keys and id are within the given table

        let sql = squel.delete()
            .table(table)
            .where(idName + ' = ?', id);

        return this.query(sql.toParam());
    }

    query(sql, values) {
        if (!this.connection)
        {
            return Promise.reject('No Connection, query not possible')
        }

        let connection = this.connection;
        return new Promise((resolve, reject) => {
            if (typeof sql == 'object'
                && sql.hasOwnProperty('text')
                && sql.hasOwnProperty('values'))
            {
                values = sql.values;
                sql = sql.text;
            }

            values = values || [];
            connection.query(sql, values, (error, results, fields) => {
                if (error)
                {
                    reject('SQL was not successful,' +
                        '\nsql: ' + sql.toString() +
                        '\nvalues: '+ values.toString() +
                        '\n'+ error);
                }

                resolve({
                    fields: fields,
                    results: results
                });
            })
        });
    }

}

module.exports = MySqlInterface;