'use strict';

let minimist = require('minimist');

class ModuleFactory {
    constructor(factory){
        this.factory        = factory || null;
        this.bootstrap      = factory && factory.bootstrap || require('core/lib/Bootstrap');
        this.logger         = factory && factory.logger || require('core/lib/LoggerFactory').getLogger(__filename);
        this.configFactory  = factory && factory.configFactory || require('core/lib/ConfigFactory');

        this._moduleList = {};

        if (this.factory == null)
        {
            let argv = minimist(process.argv.slice(2));
            let configFile = argv['conf'];

            this._coreModule = new Module(this, 'core', '', configFile);
            console.log(this.coreModule)
            console.log('called')
        }
    }

    get coreModule() {
        
        return this._coreModule || null;
    }

    addModule(name, location, configFile) {
        this._moduleList[name] = new Module(this, name, location, configFile);
    }

    addModules(modules) {
        let module = modules.shift() || {};

        if (module.hasOwnProperty('name') &&
            module.hasOwnProperty('location') &&
            module.hasOwnProperty('conf'))
        {
            this.addModule(module.name, module.location, module.conf)
        }

        if (modules.length > 0)
        {
            this.addModules(modules);
        }
    }
}

class Module {
    constructor(factory, name, location, config) {
        this.factory    = factory;
        this.config     = config[name];
        this.name       = name;
        this.module     = require(location + '/index');

        this.subModules = new ModuleFactory(factory);
        this.subModules.addModules(this.config.get('modules') || []);
    }

    start() {
        let name = this.name;
        return this.factory.bootstrap.finishedInit().catch((err) => {
            logger.fatal(`Could not run module "${name}", err: ${err}`);
        });
    }
}

module.exports = ModuleFactory;