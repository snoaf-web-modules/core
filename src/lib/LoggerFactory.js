'use strict';

let fs = require('fs');

let config      = require('./ConfigFactory').getConfig('core');
let bootstrap   = require('./Bootstrap');

const levels = [
    {
        key: 'all',
        title: 'ALL',
        color: ''
    },
    {
        key: 'trace',
        title: 'TRACE',
        color: '\x1b[0m'    // NO COLOUR Text code
    },
    {
        key: 'debug',
        title: 'DEBUG',
        color: '\x1b[36m'   // CYAN Text code
    },
    {
        key: 'info',
        title: 'INFO',
        color: '\x1b[36m'   // CYAN Text code
    },
    {
        key: 'warn',
        title: 'WARN',
        color: '\x1b[33m'   // YELLOW Text code
    },
    {
        key: 'error',
        title: 'ERROR',
        color: '\x1b[31m'   // RED Text code
    },
    {
        key: 'fatal',
        title: 'FATAL',
        color: '\x1b[31m'   // RED Text code
    },
    {
        key: 'none',
        title: 'NONE',
        color: ''
    }
];

function getlevelByKey(key) {
    for (let idx in levels)
    {
        if (levels[idx].key == key)
        {
            return levels[idx];
        }
    }
    return null;
}

const levelsByKey = levels.reduce(function(map, obj) {
    map[obj.key] = {
        title: obj.title,
        consoleColour: obj.color
    };
    return map;
}, {});

const Level = levels.reduce(function(map, obj) {
    map[obj.title] = obj.key;
    return map;
}, {});

const consoleColourClear = '\x1b[0m';

class LoggerFactory {
    constructor () {
        this.consoleLogLevel = '';
        this.fileLogLevel = '';
        this.logFile = '';

    }

    init() {
        this.consoleLogLevel = config.get('log-level') || Level.DEBUG;
        this.fileLogLevel = config.get('file-log-level') || Level.DEBUG;

        this.logFile = this._createLogFile(config.get('log-file') || '');
    }

    getLogger(fileName) {
        return new Logger(fileName);
    }

    _log(level, file, message) {
        let levelText = levelsByKey[level].title;
        let colourLevelText = levelsByKey[level].consoleColour + levelText + consoleColourClear;

        if (this._isWithinLevel(level, this.consoleLogLevel))
        {
            console.log([
                this._getFormattedDate(),
                colourLevelText,
                file,
                message].join(' - '));
        }

        if (this._isWithinLevel(level, this.fileLogLevel))
        {
            this._logInFile(this.logFile, [
                this._getFormattedDate(),
                levelText,
                file,
                message].join(' - '));
        }
    }

    _createLogFile(fileName) {

        return new Promise((resolve, reject) => {
                if (fileName != '')
                {
                    // Open file for appending ('a' flag)
                    fs.open(fileName, 'a',
                        function(err, fd) {
                            if (!err)
                            {
                                console.log('Logging File Opened');
                                resolve(fd);
                                return;
                            }
                            console.log('ERROR: Logging File Could Not Be Opened');
                            reject();
                        }
                    );
                }
            })
            .catch(() => {
                console.log("Failed to initialise Logger. " +
                    "You are having a bad day and will not go to space today.");
            });
    }

    _logInFile(file, message) {
        return file
            .then((fd) => {
                if (fd)
                {
                    fs.appendFile(fd, message + '\n', (err) => {
                        if (!err)
                        {
                            return Promise.resolve(fd);
                        }
                        return Promise.reject(fd);
                    });
                }
            })
            .catch(()=> {
                console.log('! Could not write message to file.');
            });
    }

    _isWithinLevel(level, checkLevel) {
        let levelPosition = levels.indexOf(getlevelByKey(level));
        let checkLevelPosition = levels.indexOf(getlevelByKey(checkLevel));

        return checkLevelPosition <= levelPosition;
    }

    _getFormattedDate() {
        var d = new Date();

        let date = [
            d.getFullYear(),
            ('0' + (d.getMonth() + 1)).slice(-2),
            ('0' + d.getDate()).slice(-2)
        ].join('-');

        let time = [
            ('0' + d.getHours()).slice(-2),
            ('0' + d.getMinutes()).slice(-2),
            ('0' + d.getSeconds()).slice(-2) + '.' + ('00' + d.getMilliseconds()).slice(-3)
        ].join(':');

        return [date, time].join(' ');
    }
}

let _singleton = new LoggerFactory();

class Logger {
    constructor(fileName) {
        this.parent = _singleton;
        this.fileName = fileName;
    }

    trace(message) {
        message = Array.from(arguments).join('');
        this.parent._log(Level.TRACE, this.fileName, message);
    }

    debug(message) {
        message = Array.from(arguments).join('');
        this.parent._log(Level.DEBUG, this.fileName, message);
    }

    info(message) {
        message = Array.from(arguments).join('');
        this.parent._log(Level.INFO, this.fileName, message);
    }

    warn(message) {
        message = Array.from(arguments).join('');
        this.parent._log(Level.WARN, this.fileName, message);
    }

    error(message) {
        message = Array.from(arguments).join('');
        this.parent._log(Level.ERROR, this.fileName, message);
    }

    fatal(message) {
        message = Array.from(arguments).join('');
        this.parent._log(Level.FATAL, this.fileName, message);
        process.exit(1);
    }
}

// Export module
// bootstrap.register(__filename, _singleton, _singleton.init);
_singleton.init();
module.exports = _singleton;