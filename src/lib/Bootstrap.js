'use strict';


/**
 * Bootstrap contains function in order for governing the startup of different singletons
 * This is to assure that the order of init functions is consistent
 */
class Bootstrap {

    constructor() {
        this.resolveQueue = Promise.resolve();
    }

    /**
     * This function is for registering any init functions that must be run on startup
     *
     * @param file This provides a way to see which file fails during start up
     * @param self This a reference to the context within which the function is run
     * @param initReference This is a reference to the registered initialisation function
     * @param params These are the params that should be used when invoking the initReference
     */
    register(file, self, initReference, params) {
        if (params == null)
        {
            params = [];
        }

        this.resolveQueue = this.resolveQueue.then(() => {
            return initReference.apply(self, params);
        }).catch((err) => {
            console.log('Failed During Bootstrap -', file);
            return Promise.reject(err);
        })
    }

    finishedInit() {
        return this.resolveQueue;
    }

}

let _singleton = new Bootstrap();
module.exports = _singleton;