'use strict';

module.exports = {
    bootstrap: require('./Bootstrap'),
    loggerFactory: require('./LoggerFactory'),
    configFactory: require('./ConfigFactory'),
    functions: require('./CoreFunctions'),
    api: require('./api/CoreApi'),
    context: require('./renderer/context/CoreContext'),
    renderEngine: require('./renderer/RenderEngine'),
    router: require('./router/CoreRouter'),
    sqlInterface: require('./interface/db/MySqlInterface')
};