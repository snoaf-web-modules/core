'use strict';

let express = require('express');
let path = require('path');

let logger = require('../LoggerFactory').getLogger(__filename);
let configFactory = require('../ConfigFactory');
let renderEngine = require('../renderer/RenderEngine');

class CoreRouter {

    init(module) {
        this.module = (!module || module == '' ? 'core' : module);
        let moduleConfig = configFactory.getConfig(this.module);

        this.contentLocation = moduleConfig.get('content-path');

        this.app = express();

        this.app.all('*', (req, res, next) => {
            req.ctx = req.ctx || {};
            req.ctx.module = this.module;
            next();
        });

        return Promise.resolve();
    }

    install404() {
        // This must remain the last thing done before starting the app
        this.app.all('*', (req, res, next) => {
                logger.warn('404 reached at - \'', req.url, '\'');
                res.status(404).sendFile(renderEngine.getPagePath(this.module, '404.html'));
            });
    }

    getRouter() {
        return this.app;
    }

    getContent(file) {
        return (req, res, next) => {
            res.sendFile(
                path.join(
                    this.contentLocation,
                    'views',
                    file
                ));
        };
    }
}

module.exports = CoreRouter;