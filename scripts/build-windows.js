let fs = require('fs');
let ncp = require('ncp').ncp;
ncp.limit = 16;

function moveToBuildFolder(filePath, newFilePath) {
    newFilePath = newFilePath || filePath;
    console.log(`Attempting to move ${filePath} to /build/${newFilePath}`);
        ncp(filePath, 'build/' + newFilePath, function(err) {
        if (err) { console.log(err); throw err; }
        console.log(`${filePath} has been moved to /build`)
    });
}

var deleteFolderRecursive = function(path) {
    if( fs.existsSync(path) ) {
        fs.readdirSync(path).forEach(function(file,index){
            var curPath = path + "/" + file;
            if(fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};

try
{
    fs.accessSync('build');
    deleteFolderRecursive('build');
    console.log('build was removed');
}
catch (e) {
    console.log('build was not removed');
}

fs.mkdirSync('build');

let fileList = [
    'src',
    'app.js',
    'package.json',
    'package-lock.json'
];

for (let idx = 0; idx < fileList.length; idx++)
{
    let file = fileList[idx];
    moveToBuildFolder(file);
}

// Add in appropriate config file
moveToBuildFolder('config/snoaf.conf', 'default.conf');